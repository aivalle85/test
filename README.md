# Challenge #

This Challenge .

### How to run it with docker ###
Assuming you're on the local repo path, if not change "$PWD" for proper path

```bash
docker run -d -p 80:80 --name my-apache-php-app -v "$PWD":/var/www/html php:7.2-apache
```

### How to use it without docker ###

* Add all files into root directory

### Checking ###

* Open a browser and use the server name, ip or localhost